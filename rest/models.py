import uuid
from django.utils.translation import ugettext as _
from django.db import models
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

GENDERS = (('f', _('female')), ('m', _('male')))
TITLES = [_('mr'), _('mrs')]


class Profile(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    owner = models.OneToOneField(User, on_delete=models.CASCADE)
    last_mod = models.DateTimeField(auto_now=True, null=True, blank=True)
    bio = models.TextField(max_length=500, blank=True)
    gender = models.CharField(choices=GENDERS, max_length=100, blank=True, default='')
    location = models.CharField(max_length=30, blank=True)
    birth_date = models.DateField(null=True, blank=True)
    company = models.CharField(max_length=50, blank=True)
    photo = models.ImageField(upload_to='avatars', blank=True)

    def __str__(self):
        return str(self.id)


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(owner=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()
