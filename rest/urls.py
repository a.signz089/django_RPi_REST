"""rest URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic import TemplateView
from django.views.generic.base import RedirectView
from rest_framework import routers

from .views import UserViewSet, GroupViewSet, ProfileViewSet, ExampleView
from stats.views import StatsViewSet, hello_world
from gpio_dev.views import PinViewSet

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'groups', GroupViewSet)
router.register(r'profiles', ProfileViewSet)
router.register(r'stats', StatsViewSet, base_name="stats")
router.register(r'gpio', PinViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    # url(r'^', UserView, name='user_profile'),
    url(r'^test$', ExampleView.as_view(), name='example'),
    url(r'^api/', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api-admin/', admin.site.urls, name='admin'),
    url(r'^api-hello/', hello_world, name='hello'),
    url(r'^qr/', include('qr.urls')),
]

if settings.DEVEL:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# Some base views
favicon_view = RedirectView.as_view(url='/static/favicon.ico', permanent=True)
robots_view = TemplateView.as_view(template_name="robots.txt", content_type="text/plain")

urlpatterns += [url(r'^favicon\.ico$', favicon_view, name='favicon'),
                url(r'^robots.txt$', robots_view, name='robots')]
