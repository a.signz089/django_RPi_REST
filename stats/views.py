import os
import platform
import psutil
import subprocess
import socket
from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer
from rest_framework.viewsets import ViewSet

from .serializers import Stats, StatsSerializer


@api_view(['GET', 'POST'])
def hello_world(request):
    if request.method == 'POST':
        return Response({"message": "Got some data!", "data": request.data})
    return Response({"message": "Hello, world!"})


def get_processor_info():
    cpu_info = {}
    if platform.system() == "Windows":
        cpu_info["model name"] = platform.processor()
    elif platform.system() == "Darwin":
        cpu_info["model name"] = subprocess.check_output(['/usr/sbin/sysctl', "-n", "machdep.cpu.brand_string"]).strip()
    elif platform.system() == "Linux":
        command = "cat /proc/cpuinfo"
        all_info = subprocess.check_output(command, shell=True).strip()
        for line in all_info.decode(encoding='utf-8').split("\n"):
            try:
                key, value = line.split(": ")
                cpu_info[key.strip()] = value
            except ValueError:
                pass
    return cpu_info


def pprint_memory():
    memory = psutil.virtual_memory()
    return memory._asdict()


class StatsViewSet(ViewSet):
    def list(self, request, format=None):
        hostname = socket.gethostname()
        cpu_info = get_processor_info()
        cpu_type = platform.processor()
        cpu_count = psutil.cpu_count()
        cpu_times = list(psutil.cpu_times())
        memory = pprint_memory()
        load = list(os.getloadavg())
        return Response({"hostname": hostname,
                         "system": platform.system(),
                         "cpu": cpu_info,
                         "cpu_count": cpu_count,
                         "memory": memory,
                         "load": load})
