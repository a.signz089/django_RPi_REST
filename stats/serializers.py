import os
import psutil
from rest_framework import serializers


class Stats(object):
    def __init__(self, created=None):
        self.cpu_times = list(psutil.cpu_times())
        self.memory = list(psutil.virtual_memory())
        self.load = list(os.getloadavg())


class StatsSerializer(serializers.Serializer):
    cpu_times = serializers.ListField
    memory = serializers.ListField
    load = serializers.ListField
