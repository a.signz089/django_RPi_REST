from django.conf import settings
from django.shortcuts import get_object_or_404, render

from rest_framework import generics, viewsets
from rest_framework import permissions
from rest_framework.decorators import detail_route, list_route
from rest_framework.response import Response

from .models import Pin

from .serializers import PinSerializer

from gpio_dev import my_PI

print("GPIO")
try:
    for pin in Pin.objects.all():
        print(pin, pin.id, pin.value)
        my_PI.set_pwm(pin.id, pin.value)
except:
    pass


class PinViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAdminUser,)

    queryset = Pin.objects.all()
    serializer_class = PinSerializer

    @detail_route(methods=['post', 'patch', 'put'])
    def set_pin(self, request, pk=None):
        queryset = Pin.objects.all()
        pin = get_object_or_404(queryset, pk=pk)
        serializer = PinSerializer(pin, context={'request': request})
        return Response(serializer.data)
