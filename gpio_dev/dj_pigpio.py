from django.conf import settings

import pigpio

from .models import Pin

pi = pigpio.pi(settings.GPIO_HOST, settings.GPIO_PORT)

for pin in Pin.objects.all():
    print(pin, pin.id, pin.value)
    pi.set_PWM_dutycycle(pin.id, pin.value)


def pin_set_pwm(pin=17, value=0):
    pi.set_PWM_dutycycle(pin, value)
