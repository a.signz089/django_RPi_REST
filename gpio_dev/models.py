from django.db import models
from django.conf import settings
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db.models.signals import post_save
from django.dispatch import receiver

from gpio_dev import my_PI


class Pin(models.Model):
    id = models.IntegerField(primary_key=True)
    last_mod = models.DateTimeField(auto_now=True, null=True, blank=True)
    title = models.TextField(max_length=500, blank=True)
    value = models.IntegerField(default=0,
                                validators=[MinValueValidator(0), MaxValueValidator(255)],)

    def __str__(self):
        return str(self.title)


@receiver(post_save, sender=Pin)
def update_pin_state(sender, instance, **kwargs):
    my_PI.set_pwm(pin=instance.id, value=instance.value)
