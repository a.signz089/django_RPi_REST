from django.apps import AppConfig


class GpioDevConfig(AppConfig):
    name = 'gpio_dev'
