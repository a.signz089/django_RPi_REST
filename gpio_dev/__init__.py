import atexit
from django.conf import settings

import pigpio


class Rpi:
    """A simple raspberrypi class"""
    def __init__(self):
        self.pi = pigpio.pi(settings.GPIO_HOST, settings.GPIO_PORT)

    def set_pwm(self, pin, value):
        self.pi.set_PWM_dutycycle(pin, value)

    def stop(self):
        print('stop rpi')
        self.pi.stop()

my_PI = Rpi()
