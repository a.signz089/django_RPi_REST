from io import BytesIO
from django.http import HttpResponse

import qrcode
import qrcode.image.svg


class QrCode:
    def __init__(self, data):
        self.data = data
        self.qr_code = qrcode.QRCode(version=1,
                                     error_correction=qrcode.constants.ERROR_CORRECT_L,
                                     box_size=20, border=2)
        self.qr_code.add_data(self.data)
        self.qr_code.make(fit=True)

    def make_qr(self):
        return self.qr_code.make_image()


class QrCodeSVG(QrCode):
    def make_qr(self):
        return self.qr_code.make_image(image_factory=qrcode.image.svg.SvgPathImage)


def index(request, qr_data):
    new_qr = QrCodeSVG(qr_data)
    buffer = BytesIO()
    new_qr.make_qr().save(buffer)
    response = buffer.getvalue()
    buffer.close()
    return HttpResponse(response, content_type="image/svg+xml")


def download(request, qr_data):
    response = HttpResponse(content_type='image/svg+xml')
    response['Content-Disposition'] = 'attachment; filename="qr.svg"'
    buffer = BytesIO()
    new_qr = QrCodeSVG(qr_data)
    new_qr.make_qr().save(buffer)
    svg = buffer.getvalue()
    buffer.close()
    response.write(svg)
    return response
