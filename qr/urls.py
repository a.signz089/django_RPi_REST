from django.conf.urls import url

from . import views

app_name = 'qr'
urlpatterns = [
    url(r'^(?P<qr_data>[\x00-\xff]+)/$', views.index, name='index'),
]
