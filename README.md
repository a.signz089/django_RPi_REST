# {{ Project Name }}
TODO: Write a project description

## Installation
TODO: Describe the installation process

### Django

    pyvenv venv
    . venv/bin/activate
    pip install -r requirements.txt

### Systemd
/etc/systemd/system/gunicorn.service

    [Unit]
    Description=gunicorn daemon
    Requires=gunicorn.socket
    After=network.target
    
    [Service]
    Environment=DJANGO_DEBUG=1
    Environment=DJANGO_DEVEL=1
    Environment=DJANGO_KEY='2+*%a2rh30=oub&m8sqibbfcwf^c9fbl637wf0!&mox$l@)9lf'
    PIDFile=/run/gunicorn/pid
    User=alarm
    Group=alarm
    WorkingDirectory=/home/alarm/Django
    ExecStart=/home/alarm/venv/bin/gunicorn --pid /run/gunicorn/pid rest.wsgi
    ExecReload=/bin/kill -s HUP $MAINPID
    ExecStop=/bin/kill -s TERM $MAINPID
    PrivateTmp=true
    
    [Install]
    WantedBy=multi-user.target

/etc/systemd/system/gunicorn.socket

    [Unit]
    Description=gunicorn socket
    
    [Socket]
    ListenStream=/run/gunicorn/socket
    ListenStream=0.0.0.0:8000
    ListenStream=[::]:9000
    
    [Install]
    WantedBy=sockets.target

## Usage
TODO: Write usage instructions

### Django
#### SECRET_KEY

    from django.utils.crypto import get_random_string
    
    chars = 'abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)'
    SECRET_KEY = get_random_string(50, chars)
    
    print (SECRET_KEY)

## Deploy
.git/hooks/post-receive

    #!/bin/sh
    echo "## PRODUCTION PUSH !!"
    git cat-file blob HEAD:README.md | markdown > $GIT_DIR/../README.html
    GIT_WORK_TREE=$GIT_DIR/.. git checkout -f
    
    PYTHON=~/venv/bin/python
    ## Django
    cd $GIT_DIR/..
    # Location of 'manage.py', if it exists.
    MANAGE_FILE=$(find . -maxdepth 3 -type f -name 'manage.py' -printf '%d\t%P\n' | sort -nk1 | cut -f2 | head -1)
    MANAGE_FILE=${MANAGE_FILE:-fakepath}
    # Run compilescss
    $PYTHON $MANAGE_FILE compilescss
    # Run collectstatic, cleanup some of the noisy output.
    $PYTHON $MANAGE_FILE collectstatic --noinput --traceback -i *.scss
    # Run migrations
    $PYTHON $MANAGE_FILE migrate


## Contributing
1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## History
TODO: Write history
## Credits
TODO: Write credits
## License
TODO: Write license
